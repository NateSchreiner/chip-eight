# Chip-8 Emulator (Virtual Machine)

### Past Implementation
> https://github.com/bradford-hamilton/chippy/tree/master

### Great resource for writing
> https://multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/
> https://tobiasvl.github.io/blog/write-a-chip-8-emulator/

### Reference Material
1. http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#0nnn
2. https://github.com/mattmikolay/chip-8/wiki/CHIP%E2%80%908-Instruction-Set

## Instructions
> CHIP-8 contains 35 instructions
- Each instruction is 2-bytes  
- They are stored in memory in big-endian fashion


## Implementation Details
4096 bytes of RAM? 
1. Programs should be loaded into memory starting at address `0x200`
2. Addresses `0x000` - `0x1FF` are reserved for the CHIP-8 interpreter
3. Final 352 bytes of memory are reserved for "variables and display refresh" and should not be used in CHIP-8 programs


