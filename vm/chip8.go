package vm

import (
	"encoding/hex"
	"fmt"
	"os"
	"time"
)

type VM struct {
	Memory [4096]byte
	opcode uint16
	// 16 data registers, each 8 bits in length
	// V0 to VF
	v [16]byte
	// Address register
	I uint16
	// Stack must provide space for 12
	// successive subroutine calls
	Stack [16]uint16
	// Program counter
	pc uint16
	// stack pointer
	sp uint16
	// Represents window pixels.
	gfx [64 * 32]byte
	// 8-bit delay timer which counts down at 60 hertz
	// (1000 / 60) until it reaches 0
	delayTimer byte

	keypad [16]byte
	// Don't draw on every CPU cycle. set flag
	// when update needed
	drawFlag bool
	// CPU "clock"
	Clock *time.Ticker
	// channel for sending / receiving shutdown signal
	ShutdownC chan struct{}
}

func (v *VM) loadRom(path string) {
	b, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}

	for i := 0; i < len(b); i++ {
		if i == 0 || i == 1 {
			hex := hex.EncodeToString([]byte{b[i]})
			fmt.Printf("Found byte: %s\n", hex)
		}
		v.Memory[0x200+i] = b[i]
	}
}

func (v *VM) Fetch() {
	first := v.Memory[v.pc]
	second := v.Memory[v.pc+1]
	fmt.Printf("op: (%s, %s)\n", hex.EncodeToString([]byte{first}), hex.EncodeToString([]byte{second}))
	// fmt.Printf("OpBfore: (%s, %s)\n", hex.EncodeToString([]byte{first}), hex.EncodeToString([]byte{second}))
	op := uint16(first)<<8 | uint16(second)
	hex.EncodeToString(op)
	fmt.Printf("Received: %s\n\n", []byte{op})

}

func NewVM(rompath string) *VM {
	vm := &VM{
		Memory: [4096]byte{},
		v:      [16]byte{},
		pc:     0x200,
		gfx:    [64 * 32]byte{},
		Clock:  time.NewTicker(time.Second / time.Duration(60)),
	}

	vm.loadRom(rompath)

	return vm
}

/*
// emulateCycle runs a full fetch, decode, and execute cycle.
// One opcode is 2 bytes long (ex. 0xA2FO) so we need to fetch two successive bytes (ex. 0xA2 and 0xF0) and merge them
// to get the actual opcode. First we shift current instruction left 8 (ex. from 10100010 -> 1010001000000000)
// Then we OR it with the upcoming byte which gives us a 16 bit chunk containing the combined bytes
*/
