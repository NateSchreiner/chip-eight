package main

import (
	"fmt"

	"gitlab.com/NateSchreiner/chip-eight/vm"
)

func main() {
	vm := vm.NewVM("./pong.ch8")
	fmt.Println(vm.Memory[0x200 : 0x200+2])
	vm.Fetch()
	// for {
	// 	vm.Fetch()
	// }
}
